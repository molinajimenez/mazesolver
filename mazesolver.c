/*
  Blank Simple Project.c
  http://learn.parallax.com/propeller-c-tutorials 
*/
#include "simpletools.h"                      // Include simple tools
#include "stdbool.h"
#include "ping.h"
#include "abdrive.h"
int main(){
  //retorna false si no tiene una pared cercana a el...
  bool checkDistancia(void){
    bool puedo;
    puedo=true;
    if(ping_cm(8)>=15){
      puedo=false;
    }      
      else{
        puedo=true;
      }
      return puedo;             
  }
  //retorna false si no tiene una pared al lado cerca...
  bool checkRight(void){
    bool puedo;
    puedo=true;
    if(ping_cm(7)>=15){
      puedo=false;
    }      
      else{
        puedo=true;
      }
      return puedo; 
  }   
  //gira izq 
  void turnLeft(void){
    drive_goto(-25,26);
  }
   //gira derecha
   void turnRight(void){
    drive_goto(26,-25);
  }          
  //loop
  while(1){
    bool enfrente = checkDistancia();
    bool derecha = checkRight();
    
    //si no tenes nada enfrente y tenes algo a la derecha... go forward.
    if((enfrente == false && derecha == true)){
      drive_goto(64,64);
      checkDistancia();
      checkRight();
    }
    //si no tenes nada enfrente ni a la derecha, trust the force luke!
    if((enfrente==false && derecha == false)){
      drive_goto(64,64);
      checkDistancia();
      checkRight();
    }
    //si tenes algo efrente y algo a la derecha, estas alejado.... RIGHT
    if((enfrente==true && derecha == false)){
      turnRight();
      checkDistancia();
      checkRight();
    }
    //si tenes algo enfrente y algo a la derecha, estas atrapado LEFT
    if((enfrente==true && derecha == true)){
      turnLeft();
      checkDistancia();
      checkRight();
    }                
    
              

  }
}